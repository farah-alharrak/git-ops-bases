#!/bin/bash
set -e

DIR=$(dirname "$0")
echo "👮 Install SealedSecret Controller"

pushd $DIR
set -x
kubectl apply -f sealed-secret-ctrl-v0.17.5.yml --wait


{ set +x; } 2> /dev/null # silently disable xtrace
echo "Get public cert for sealing secret"

# waiting...
echo "⏳ Waiting for public cert"
while [ "" == "$(kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o json | jq -r '.items[] | has("kind")')" ]; do echo -n "."; sleep 1; done
echo " ✅"

echo "📦 Save public cert in $(pwd)/public-cert.pem"
set -x
kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o jsonpath="{.items[0].data['tls\.crt']}" | base64 -d > public-cert.pem 

popd
