# Helm

[Helm](https://helm.sh/) est un gestionnaire de paquets pour Kubernetes. Celui-ci a été préinstallé dans votre livre de sort.

Il y a 3 concepts à connaître sur Helm:

- Un **Chart** est un package Helm. Il va contenir tout ce qui va être nécessaire afin d'installer l'application voulue. Vous pouvez comparer ça à un RPM RedHat ou un DEB Debian.
- Un **Repo** est un emplacement ou les **Charts** peuvent être distribués de façon similaire à npmjs ou maven central.
- Une **Release** est une instance d'un **Chart** déployée dans un cluster Kubernetes. A chaque fois que vous installez un **Chart** dans un cluster Kubernetes, une **Release** est crée. Donc si vous installez 2 fois le même **Chart** dans un cluster Kubernetes, vous aurez donc 2 **Releases**.

## Autocompletion commande helm sur bash

https://helm.sh/docs/helm/helm_completion_bash/

## Recherche de Charts sur le hub

```sh
$ helm search hub <text>
```

## Installer un Chart (=> release)

```sh
$ helm install <release-name> <chart>
```

## Personaliser l'installation d'un Chart (=> release)

```sh
$ cat values.yml
...
$ helm install -f values.yml <release-name> <chart>
```

## Generer les manifest "standard" d'un Chart helm (sur la sortie console)

```sh
$ cat values.yml
...
$ helm template -f values.yml <release-name> <chart>
```

Si l'on chaine cela avec ` | kubectl apply -f-` cela installe le resultat produit par le chart **sans créer de release**

## Avoir la liste des releases

```sh
$ helm ls
```

## Avoir le statut d'une release

```sh
$ helm status <release-name>
```

## Voir les options d'un Chart

```sh
$ helm show values <chart>
```

## Options communes

Dans les commandes de manipulation des charts il y a des options commune comme par exemple `-n` pour spécifier le namespace cible

## Guide sur le developpements de Charts

https://helm.sh/docs/topics/charts/

## Helm best practices

https://codersociety.com/blog/articles/helm-best-practices
