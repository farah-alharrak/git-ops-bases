# K9s

(K9s)[https://github.com/derailed/k9s] est une interface dans le terminal permettant de visualiser et modifier des ressources déployé dans un cluster kubernetes.  
La navigation utilise les même raccourcis clavier de **vim** 🥰 (et pas emacs )

Pour lancer K9S depuis votre terminal il y a 2 conditions :

- avoir le binaire k9s
- avoir un "kubeconfig" (comme pour kubectl)

## Navigation

Pour changer le type d'objet que vous voulez voir (par exemple voir les `deployements`) tapez

```
<esc> <:> <deploy> <enter>
```

Pour filtrer dans la liste (par exemple ce qui contient par dns) tapez

```
<esc> </> <dns> <enter>
```

Autres raccourcits claviers :

- Editer (l'objet que vous selectionnez): `<e>`
- Enregister: `<w>`
- Quitter : `<q>`
- Voir les logs d'un pod selectionné : `<l>`
- Visualiser (et selectionner) la liste des type d'objet définit dans le cluster: `<SHIFT A>`

Et il existe plein d'autres commandes utiles

## Screen shot

````
 Context: gandalf-cluster                          <0> all       <a>      Attach     <l>       Logs        ____  __.________
 Cluster: gandalf-cluster                          <1> default   <ctrl-d> Delete     <p>       Logs Previo|    |/ _/   __   \______
 User:    gandalf-cluster                                        <d>      Describe   <shift-f> Port-Forwar|      < \____    /  ___/
 K9s Rev: v0.26.5 ⚡️v0.26.6                                      <e>      Edit       <s>       Shell      |    |  \   /    /\___ \
 K8s Rev: v1.22.11+k3s1                                          <?>      Help       <n>       Show Node  |____|__ \ /____//____  >
 CPU:     n/a                                                    <ctrl-k> Kill       <f>       Show PortFo        \/            \/
 MEM:     n/a
┌────────────────────────────────────────────────────────── Pods(all)[7] ──────────────────────────────────────────────────────────┐
│ NAMESPACE↑   NAME                                         PF READY RESTARTS STATUS   IP           NODE                           │
│ kube-system  civo-ccm-7cb9c4b58f-r9rjq                    ●  1/1          0 Running  192.168.1.3  k3s-gandalf-cluster-b1ff-fd6fe │
│ kube-system  civo-csi-controller-0                        ●  4/4          0 Running  10.42.0.2    k3s-gandalf-cluster-b1ff-fd6fe │
│ kube-system  civo-csi-node-gvr5x                          ●  2/2          0 Running  192.168.1.3  k3s-gandalf-cluster-b1ff-fd6fe │
│ kube-system  civo-csi-node-hp8rc                          ●  2/2          0 Running  192.168.1.4  k3s-gandalf-cluster-b1ff-fd6fe │
│ kube-system  coredns-7796b77cd4-tnt72                     ●  1/1          0 Running  10.42.0.3    k3s-gandalf-cluster-b1ff-fd6fe │
│ kube-system  metrics-server-ff9dbcb6c-qkwgf               ●  1/1          0 Running  10.42.0.4    k3s-gandalf-cluster-b1ff-fd6fe │
│ kube-system  traefik-ingress-controller-69b8bd4464-5fkng  ●  1/1          0 Running  10.42.1.2    k3s-gandalf-cluster-b1ff-fd6fe │
│                                                                                                                                  │
│                                                                                                                                  │
│                                                                                                                                  │
│                                                                                                                                  │
│                                                                                                                                  │
│                                                                                                                                  │
│                                                                                                                                  │
│                                                                                                                                  │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
  <pod>                                                                                              ```
````
