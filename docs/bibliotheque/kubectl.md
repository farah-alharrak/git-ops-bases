# Kubectl

## Aliases utiles pour kubectl

```sh
echo 'alias k=kubectl' >>~/.bashrc
echo 'alias kg="kubectl get"' >>~/.bashrc
echo 'complete -o default -F __start_kubectl k' >>~/.bashrc
source ~/.bashrc
```

**Note** Vous pouvez aussi utiliser des bibliothèques d'alias existantes comme https://github.com/ahmetb/kubectl-aliases

## Ajouter l'auto-completion bash de kubectl

```sh
echo 'source <(kubectl completion bash)' >>~/.bashrc
source ~/.bashrc
```

## A connaitre

https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands référence toutes les commandes avec des explications et exemple. C'est la référence à avoir

Quelques commandes essentielles:
**apply**

Apply a configuration to a resource by file name or stdin. The resource name must be specified. This resource will be created if it doesn't exist yet. To use 'apply', always create the resource initially with either 'apply' or 'create --save-config'.

Usage

```
$ kubectl apply (-f FILENAME | -k DIRECTORY)
```

**delete**
Delete resources by file names, stdin, resources and names, or by resources and label selector.

JSON and YAML formats are accepted. Only one type of argument may be specified: file names, resources and names, or resources and label selector.

Usage

```
$ kubectl delete ([-f FILENAME] | [-k DIRECTORY] | TYPE [(NAME | -l label | --all)])
```

**get**
Display one or many resources.

Prints a table of the most important information about the specified resources. You can filter the list using a label selector and the --selector flag. If the desired resource type is namespaced you will only see results in your current namespace unless you pass --all-namespaces.

Usage

```
$ kubectl get [(-o|--output=)json|yaml|name|go-template|go-template-file|template|templatefile|jsonpath|jsonpath-as-json|jsonpath-file|custom-columns|custom-columns-file|wide] (TYPE[.VERSION][.GROUP] [NAME | -l label] | TYPE[.VERSION][.GROUP]/NAME ...) [flags]
```
